const defaultTheme = require('tailwindcss/defaultTheme');
const production = !process.env.ROLLUP_WATCH;

module.exports = {
  purge: {
    content: ['./src/**/*.svelte'],
    enabled: production,
  },
  variants: {
    extend: {
      backgroundColor: ['active'],
      textColor: ['visited'],
    },
  },
  theme: {
    extend: {
      colors: {
        ocean: '#04131A',
        orange: '#F36523',
      },
      backgroundColor: {
        ocean: '#04131A',
        aquamarine: '#063447',
      },
      textColor: {
        orange: '#F36523',
        'hacker-green': '#00F427',
      },
      borderColor: {
        orange: '#F36523',
      },
      fontFamily: {
        sans: ['Poppins', ...defaultTheme.fontFamily.sans],
        body: ['Poppins', ...defaultTheme.fontFamily.sans],
      },
    },
  },
};
